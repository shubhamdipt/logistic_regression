import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


def analyse():
    df = pd.read_csv("agaricus-lepiota.data.txt")
    columns = [
        "cap_shape",
        "cap_surface",
        "cap_color",
        "bruises",
        "odor",
        "gill_attachment",
        "gill_spacing",
        "gill_size",
        "gill_color",
        "stalk_shape",
        "stalk_root",
        "stalk-surface-above-ring",
        "stalk-surface-below-ring",
        "stalk-color-above-ring",
        "stalk-color-below-ring",
        "veil-type",
        "veil-color",
        "ring_number",
        "ring_type",
        "spore-print-color",
        "population",
        "habitat",
    ]
    df.columns = ["label"] + columns
    df = df.replace({'label': 'e'}, 0)
    df = df.replace({'label': 'p'}, 1)
    df = pd.get_dummies(df, prefix=columns)
    x_train, x_test, y_train, y_test = train_test_split(
        df.loc[:, df.columns != 'label'],
        df["label"],
        test_size=0.3
    )
    regr = LogisticRegression()
    regr.fit(x_train, y_train)
    y_pred = regr.predict(x_test)
    print("Accuracy score %:", accuracy_score(y_test, y_pred)*100)


if __name__ == "__main__":
    analyse()